import MediumEditor from 'medium-editor'
import { debug } from 'phoenix_live_view'

export function buildEditableTextHook(baseline) {
  return {
    mounted(param1) {
      console.log('mounted', this)
      let form = this.el.closest("form")
      let targetInput = form.querySelector(`[name="${this.el.dataset.name}"]`)

      console.log('targetInput', targetInput)

      this.el.addEventListener("input", e => {
        // push event to the server
        // this.pushEvent("update_event", {content: this.el.innerText})
        // or copy to hidden input and trigger parent form event
        console.log('got input', this.el.innerHTML)
        // targetInput.value = this.el.innerHTML
        this.pushEvent("body_changed", this.el.innerHTML)
        // targetInput.dispatchEvent(new Event("input", {bubbles: true}))
      }, false)

      // this.handleEvent("inc", (payload) => {
      //   console.log('got event inc with payload', payload)
      // })
      // this.handleEvent("phx_reply", (payload) => {
      //   console.log('got event phx_reply with payload', payload)
      // })
      new MediumEditor([this.el])

      // readMore(this.el, baseline)
    },
    updated(param1, param2) {
      console.log('updated', param1, param2)
      // new MediumEditor([this.el])
      // console.log("read more")
      // readMore(this.el, baseline)
    }
  }
}
