defmodule JustThisPage.CreatePostTest do
  use ExUnit.Case, async: true
  use Wallaby.Feature

  import Wallaby.Query

  feature "New user creates a draft post and publishes it", %{session: session} do
    session
    |> create_new_post()
    |> write_paragraph("I am happy to join with you today in what will go down in history as the greatest demonstration for freedom in the history of our nation.")
  end

  defp create_new_post(session) do
    session
    |> visit("/")
    |> click(link("create a page"))
  end

  defp then_sleep_for(session, duration) do
    Process.sleep(duration)
    session
  end

  defp focus_element(session, id) do
    session
    |> execute_script("document.getElementById('#{id}').focus()")
    |> then_sleep_for(1000)
  end

  defp silent_send_keys(session, text) do
    logger = Application.get_env(:wallaby, :js_logger)
    Application.put_env(:wallaby, :js_logger, nil)

    result = session |> send_keys(text)

    Application.put_env(:wallaby, :js_logger, logger)
    result
  end

  defp write_paragraph(session, text) do

    session
    |> focus_element("post-contenteditable")
    |> silent_send_keys(text)
  end
end
