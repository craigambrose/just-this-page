defmodule JustThisPostWeb.FeatureCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      use Wallaby.DSL
    end
  end

  # using do
  #   quote do
  #     use Wallaby.DSL

  #     alias JustThisPage.Repo
  #     import Ecto
  #     import Ecto.Changeset
  #     import Ecto.Query

  #     import JustThisPageWeb.Router.Helpers
  #   end
  # end

  # setup tags do
  #   :ok = Ecto.Adapters.SQL.Sandbox.checkout(JustThisPage.Repo)

  #   unless tags[:async] do
  #     Ecto.Adapters.SQL.Sandbox.mode(JustThisPage.Repo, {:shared, self()})
  #   end

  #   metadata = Phoenix.Ecto.SQL.Sandbox.metadata_for(JustThisPage.Repo, self())
  #   {:ok, session} = Wallaby.start_session(metadata: metadata)
  #   {:ok, session: session}
  # end
end
