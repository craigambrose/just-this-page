use Mix.Config

port = String.to_integer(System.get_env("PORT") || "4000")
default_secret_key_base = :crypto.strong_rand_bytes(43) |> Base.encode64()

config :just_this_post, JustThisPostWeb.Endpoint,
  http: [port: port],
  url: [scheme: "https", host: "justthis.page", port: 443],
  secret_key_base: System.get_env("SECRET_KEY_BASE") || default_secret_key_base
