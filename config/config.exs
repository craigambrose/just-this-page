# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :just_this_post,
  ecto_repos: [JustThisPost.Repo]

# Configures the endpoint
config :just_this_post, JustThisPostWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "cZaRh7mJGrawgde1McCSvTb9/oUdSGkbi0btFMQrO277elvQ+ruMkfSt1fhzLOpM",
  render_errors: [view: JustThisPostWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: JustThisPost.PubSub,
  live_view: [signing_salt: "OEAjR+lo"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
